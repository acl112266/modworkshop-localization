<?php
$l['cookielaw_header'] = 'Cookies';
$l['cookies_title'] = "🍪Cookies";
$l['cookies_settings'] = "Ustawienia Cookies";
$l['cookies_desc'] = "Używamy plików cookie, aby zwiększyć komfort korzystania z witryny. Więcej informacji znajdziesz w naszej Polityce dotyczącej plików cookie.";
$l['cookielaw_allow'] = 'Zezwól';
$l['cookielaw_disallow'] = 'Odmów';
$l['cookielaw_disallow_confirm'] = "Czy na pewno chcesz zabronić używania plików cookie? Bez plików cookie nie będzie można się zalogować, a niektóre funkcje strony będą ograniczone.";
$l['cookielaw_more_info'] = 'Więcej Informacji';
$l['cookielaw_info_title'] = 'Zgoda Na Pliki Cookies';
$l['cookielaw_info_cookie_name'] = 'Nazwa Cookie';
$l['cookielaw_info_cookie_description'] = 'Opis Cookie';
$l['cookielaw_info_cookies_set_logged_in'] = 'Ustaw po zalogowaniu';
$l['cookielaw_info_cookies_set_guest'] = 'Ustaw podczas przeglądania jako gość';
$l['cookielaw_cookie_sid_desc'] = 'Identyfikator bieżącej sesji.';
$l['cookielaw_cookie_mybbuser_desc'] = 'Twój identyfikator użytkownika i klucz logowania, używane do zalogowania się.';
$l['cookielaw_cookie_forumpass_desc'] = 'Hasło używane na forach chronionych hasłem.';
$l['cookielaw_cookie_collapsed_desc'] = 'Które kategorie forum zostały zwinięte, aby strona je zapamiętała.';
$l['cookielaw_cookie_multiquote_desc'] = 'Posty, które wybrałeś do multicytowania';
$l['cookielaw_cookie_mybblang_desc'] = 'Język strony.';
$l['cookielaw_cookie_mybbtheme_desc'] = 'Motyw strony (w tej chwili nieużywany).';
$l['cookielaw_cookie_coppauser_desc'] = 'Niezależnie od tego, czy jesteś użytkownikiem COPPA.';
$l['cookielaw_cookie_coppadob_desc'] = 'Data urodzenia dla użytkowników COPPA';
$l['cookielaw_cookie_loginattempts_desc'] = 'Nieudane próby zalogowania się.';
$l['cookielaw_cookie_failedlogin_desc'] = 'Nieudane logowanie do witryny.';
$l['cookielaw_cookie_mybbratethread_desc'] = 'Wątki, w których już głosowałeś.';
$l['cookielaw_cookie_mybb[lastvisit]_desc'] = 'Twoja ostatnia wizyta na forum.';
$l['cookielaw_cookie_mybb[lastactive]_desc'] = 'Kiedy ostatnio byłeś aktywny na forum.';
$l['cookielaw_cookie_mybb[threadread]_desc'] = 'Wątki, które widziałeś i czytałeś.';
$l['cookielaw_cookie_mybb[forumread]_desc'] = 'Fora oznaczone jako przeczytane.';
$l['cookielaw_cookie_mybb[readallforums]_desc'] = 'Jeśli wszystkie fora zostały oznaczone jako przeczytane.';
$l['cookielaw_cookie_mybb[announcements]_desc'] = 'Ogłoszenia, które przeczytałeś.';
$l['cookielaw_cookie_mybb[referrer]_desc'] = 'Identyfikator osoby polecającej użytkownika, który polecił Ci się zalogować (aktualnie nieużywany).';
$l['cookielaw_cookie_fcollapse_desc'] = 'Zwinięte dokumenty Pomocy.';
$l['cookielaw_cookie_pollvotes_desc'] = 'Śledzenie głosów w ankiecie.';
$l['cookielaw_cookie_allow_cookies_desc'] = "Niezależnie od tego, czy masz włączone pliki cookie. <strong> Uwaga: ten plik cookie jest ustawiany niezależnie od dokonanego wyboru i ma na celu zapobieganie ponownemu pytaniu przy każdej wizycie.</strong>";
$l['cookielaw_cookie_inlinemod_*_desc'] = 'Śledzi, które wątki / posty wybrałeś do wbudowanego moderowania.';
$l['cookielaw_cookie_adminsid_desc'] = 'Utrzymuje zalogowanie do administracyjnego panelu sterowania.';
$l['cookielaw_cookie_acploginattempts_desc'] = 'Nieudane próby zalogowania się do administracyjnego panelu sterowania.';
$l['cookielaw_cookie_acpview_desc'] = 'Sposób wyświetlania listy użytkowników w administracyjnym panelu sterowania.';
$l['cookielaw_cookie_theme_color_desc'] = 'Wybrany motyw.';
$l['cookielaw_cookie_mods_displaymode_desc'] = 'Sposób wyświetlania modów. Siatka, lista lub lista bez miniatur.';
