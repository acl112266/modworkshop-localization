<?php
$l['username'] = 'Username';
$l['avatar_image_warning'] = '최대 파일 크기: 100KiB. 사용 가능한 타입: jpeg, png, gif, webp, 그리고 apng.200x200 픽셀 크기 권장.';
$l['banner_image_warning'] = '최대 파일 크기: 5MiB. 사용 가능한 타입: jpeg, png, gif, webp, 그리고 apng. 1500x300 픽셀 크기 권장.';
$l['supporters_only'] = "지원자 전용";
$l['custom_user_color'] = "커스텀 사용자 색상 (Hex Color, 공백으로 놔둬 기본 설정으로 하십시요)";
$l['custom_user_title'] = "유저 타이틀";
$l['bio'] = "자기 소개";
$l['availability'] = "가용성";
$l['avatar'] = "Avatar";
$l['images_in_posts'] = "포스트에 이미지 표시";
$l['videos_in_posts'] = "포스트에 동영상 표시";
$l['signatures_in_posts'] = "포스트에 사용자의 서명 표시";
$l['avatars_in_posts'] = "포스트에 유저의 아바타 표시";
$l['away_reason'] = "부재중 이유";
$l['away_toggle'] = "전 현제 부재중이고 지금 바쁩니다.";
$l['invisible_mode'] = "다른 사람의 온라인 리스트로부터 숨깁니다.";
$l['pms_from_users'] = "사용자들로부터 개인 매시지 수신";
$l['show_pm_notice'] = "읽지 않은 개인 매시지 알림";
$l['forum'] = "포럼";
$l['banner'] = "배너";
$l['automatic'] = "자동";
$l['always'] = "항상 허용";
$l['never'] = "절대로 거부";
$l['settings_link_warning'] = "이 섹션에 링크는 메뉴를 새로고침합니다. 변경 전에 미리 변경점을 저장하도록 하십시오.";
$l['username_error_empty'] = '유저네임은 공백으로 할수 없습니다!'; 
$l['private_profile'] = '비공개 프로필 (오직 당신과 관리자들만 볼수 있습니다)'; 
$l['hide_steam_link'] = 'Steam 프로필 링크 숨기기'; 
$l['auto_set_username'] = '로그인 마다 Steam의 유저네임으로 자동 지정'; 
$l['auto_set_avatar'] = '로그인 마다 Steam의 아바타로 자동 지정'; 
$l['dstcorrection'] = "일광 절약 시간제 보정";
$l['time_and_date'] = "시간과 날짜";
$l['joinable_roles'] = "참여가능한 역할";
$l['leading_roles'] = "역할";
$l['set_as_display'] = "표시 역할로 지정";
$l['in_roles'] = "현제 할당된 역할";
$l['signature'] = "서명";
$l['roles'] = "역할";
$l['role'] = "역할";
$l['join'] = "참여하기";
$l['leave'] = "나가기";
$l['timezone'] = "타임존";
$l['time_format'] = "시간 형식";
$l['date_format'] = "날짜 형식";
$l['conditions'] = "정황";
$l['threads_per_page'] = "페이지 당 스레드";
$l['posts_per_page'] = "페이지 당 포스트";
$l['usergroup_joins_moderated'] = "그룹 리더는 이 그룹에 참여를 관리해야합니다.";
$l['usergroup_joins_invite'] = "그룹 리더가 먼저 초대해야 이 그룹에 들어올수 있습니다.";
$l['usergroup_joins_anyone'] = "누구든 마음껏 이 그룹에 들어올수 있습니다.";
$l['display_group'] = "그룹 표시";
$l['view_requests'] = "참여 요청";
$l['join_requests'] = "보류중인 참여 요청";
$l['myalerts_setting_quoted'] = '포스트에 인용될 때 알림을 받으시겠습니까?';
$l['myalerts_setting_post_threadauthor'] = '누군가 당신의 스레드에 답변을 할때 알림을 받으시겠습니까?';
$l['myalerts_setting_subscribed_thread'] = '누군가 당신이 구독한 스레드에 답변을 할때 알림을 받으시겠습니까?';
$l['myalerts_setting_rated_threadauthor'] = '누군가 당신의 스레드에 평점을 매길때 때 알림을 받으시겠습니까?';
$l['myalerts_setting_voted_threadauthor'] = "누군가 당신의 스레드의 여론 조사에서 투표를 할때 알림을 받으시겠습니까?";
$l['myalerts_setting_mydownloads_new_comment'] = '누군가 당신의 모드에 댓글을 달때 알림을 받으시겠습니까?';
$l['myalerts_setting_mydownloads_comment_mention'] = '누군가 당신을 댓글로 언급할 때 알림을 받으시겠습니까?';
$l['myalerts_setting_alert_mod_updated'] = '팔로우 중인 모드가 업데이트 될때 알림을 받으시겠습니까?';
