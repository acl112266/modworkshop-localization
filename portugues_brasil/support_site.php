<?php

$l['already_support'] = "Você já apoiou a gente, muito obrigado mesmo! ❤"; 
$l['support_mws_details'] = "
<h4>Odeia anúncios? Quer apoiar a gente??</h4>

Sem anúncios.<br>
Um cargo no Discord e no site.<br>
Um símbolo mostrando que você apoia a gente enquanto navega.<br>
Cor personalizada para o seu nome no site e no nosso servidor do Discord*<br>
";
$l['support_mws_extra_details'] = "
*No Discord, você ainda vai ter que pedir pela cor personalizada para um moderador.<br>
*Para ter o seu reembolso ou extender o plano, por favor fale com um adminstrador.<br>
*Nosso site é focado em inglês você vai ter o melhor atedimento nesse idioma.<br>
";
$l['support_mws_not_logged'] = 'Para apoiar a gente, você deve estar logado. <a href="/login">Se registre atráves do Steam.</a>';
